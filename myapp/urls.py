from django.urls import path
from .views import booklist, data

app_name = 'myapp'

urlpatterns = [
    path('', booklist, name='booklist'),
    path('data/', data, name='data'),
]
